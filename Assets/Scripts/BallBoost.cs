﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BallBoost : MonoBehaviour {

    private Rigidbody2D rb;

    int sortXPos;

    public bool Restart = false;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Use this for initialization
    void Start() {
        sortXPos = Random.Range(1, 3);

        float sortYPos = Choose<float>(-0.01f, 0.01f);

        if (sortXPos == 1) {
            rb.AddForce(new Vector2(0.02f, sortYPos));
        }
        else {
            rb.AddForce(new Vector2(-0.02f, sortYPos));
        }

    }

    // Update is called once per frame
    void Update() {
        if (Restart)
        {

            sortXPos = Random.Range(1, 3);

            float sortYPos = Choose<float>(-0.01f, 0.01f);

            if (sortXPos == 1)
            {
                rb.AddForce(new Vector2(0.02f, sortYPos));
            }
            else
            {
                rb.AddForce(new Vector2(-0.02f, sortYPos));
            }
            Restart = false;
        }

        StartCoroutine(Velocity());

        if (Score.scoreP1 >= 21 || Score.scoreP2 >= 21)
        {
            if (Input.GetKeyDown(KeyCode.Space) && Time.timeScale == 0)
            {
                Restart = true;
                Time.timeScale = 1;
            }
        }
            
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Rigidbody2D rbc = collision.gameObject.GetComponent<Rigidbody2D>();

        if (collision.gameObject.tag == "Player") {
            rb.velocity = rb.velocity + (rbc.velocity / 15);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "RightBar")
        {
            if (Score.scoreP1 > 0 && Score.scoreP1 % 5 == 0)
            {
                Score.instBall = true;
            }
            Score.scoreP1++;
        }
        else if (collision.gameObject.tag == "LeftBar")
        {
            if (Score.scoreP2 > 0 && Score.scoreP2 % 5 == 0)
            {
                Score.instBall = true;
            }
            Score.scoreP2++;
        }
    }

    public T Choose<T>(T a, T b, params T[] p)
    {
        int random = Random.Range(0, p.Length + 2);
        if (random == 0) return a;
        if (random == 1) return b;
        return p[random - 2];

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "RightBar" || collision.gameObject.tag == "LeftBar")
        {
            transform.position = new Vector3(0, 0, transform.position.z);
            rb.velocity = new Vector2(0, 0);
            Time.timeScale = 0;
            StartCoroutine(Wait());

        }
    }

    IEnumerator Wait()
    {
        Time.timeScale = 1;
        yield return new WaitForSeconds(Random.Range(1.5f, 2));
        Restart = true;
        
    }

    IEnumerator Velocity()
    {
        rb.velocity = rb.velocity * 1.0002f;
        yield return new WaitForSeconds(0.5f);

    }
}
