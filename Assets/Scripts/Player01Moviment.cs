﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player01Moviment : MonoBehaviour {

    private float speed = 7;

    private bool goingUp;
    private bool goingDown;

    private Rigidbody2D rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Use this for initialization
    void Start () {
		
	}

    // Update is called once per frame
    void Update() {
        if (Input.GetKey(KeyCode.W)) {
            goingUp = true;
        }
        else
        {
            goingUp = false;
        }

        if (Input.GetKey(KeyCode.S))
        {
            goingDown = true;
        }
        else
        {
            goingDown = false;
        }
    }

    private void FixedUpdate()
    {
        if (goingUp)
        {
            rb.velocity = new Vector2(0, speed);
        }
        else if (goingDown)
        {
            rb.velocity = new Vector2(0, speed * (-1));
        }
        else
        {
            rb.velocity = new Vector2(0, 0);
        }
    }
}
