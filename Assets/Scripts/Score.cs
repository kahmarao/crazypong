﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Score : MonoBehaviour {

    public static int scoreP1;
    public static int scoreP2;

    public Text ScoreP1;
    public Text ScoreP2;

    public GameObject p1Win;
    public GameObject p2Win;

    public GameObject ball;

    public static bool instBall = true;

    public Vector3 origin = new Vector3(0, 0);

    // Use this for initialization
    void Start () {
    
	}
	
	// Update is called once per frame
	void Update () {
        ScoreP1.text = scoreP1.ToString();

        ScoreP2.text = scoreP2.ToString();

        if (scoreP1 >= 21)
        {
            p1Win.SetActive(true);
            Time.timeScale = 0;

            if (Input.GetKeyDown(KeyCode.Space) && Time.timeScale == 0)
            {
                Time.timeScale = 1;
                scoreP1 = 0;
                scoreP2 = 0;
                Destroy(ball);
                p1Win.SetActive(false);
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }

        if (scoreP2 >= 21)
        {
            p2Win.SetActive(true);
            Time.timeScale = 0;


            if (Input.GetKeyDown(KeyCode.Space) && Time.timeScale == 0)
            {
                Time.timeScale = 1;
                scoreP1 = 0;
                scoreP2 = 0;
                Destroy(ball);
                p2Win.SetActive(false);
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }

        }

        if ((scoreP1 > 0 && scoreP1 % 3 == 0) || (scoreP2 > 0 && scoreP2 % 3 == 0))
        {
            if (instBall)
            {
                GameObject balls =  Instantiate(ball);
                balls.transform.position = origin;
                instBall = false;
            }
        }
    }
}
